# poc-nestjs-lambda-ts

## Description

This POC allows to deploy a Nestjs app on lambda

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Package the lambda

```bash
AWS_SDK_LOAD_CONFIG=1 sls package --verbose
```

## Test the lambda locally

```bash
AWS_SDK_LOAD_CONFIG=1 sls offline --verbose
# Then go to http://localhost:3000/dev/users
```

## Deploy the lambda

```bash
AWS_SDK_LOAD_CONFIG=1 sls deploy --verbose
```

## Remove the CloudFormation and all its resources

```bash
AWS_SDK_LOAD_CONFIG=1 sls remove --verbose
```
