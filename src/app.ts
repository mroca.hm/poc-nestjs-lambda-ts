import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AbstractHttpAdapter } from '@nestjs/core/adapters/http-adapter';

export async function createApp(httpAdapter?: AbstractHttpAdapter) {
  const app = await NestFactory.create(AppModule, httpAdapter);

  const config = new DocumentBuilder()
    .setTitle('Poc example')
    .setDescription('The poc API description')
    .setVersion('1.0')
    .addTag('poc')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  return app;
}
