import { Controller, Get, NotFoundException, Param } from '@nestjs/common';

@Controller()
export class UserController {
  @Get('/users')
  getUsersList() {
    return [
      { id: 1, name: 'Foo' },
      { id: 2, name: 'Bar' },
    ];
  }
  @Get('/users/:id')
  getUser(@Param('id') id: number): object {
    console.log(id);
    const user = this.getUsersList()[id - 1];
    if (!user) {
      throw new NotFoundException();
    }

    return user;
  }
}
